import React from "react";
import NFTCard from "./NFTCard";


const NFTContrainer = ({nftList,stakeNft}) => {
    return (
        <div>
            {nftList.map((nft, index) => {
                return <NFTCard nft={nft} key={index} stakeNft={stakeNft}/>
            })}
        </div>
    )
}

export default NFTContrainer;