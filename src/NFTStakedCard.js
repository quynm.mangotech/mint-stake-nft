import React from "react";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

const NFTStakedCard = ({tokenId, unStakeNft}) => {
    return (
    <Card  style={{ width: '20rem', display: "inline-block", marginRight: "2rem", marginBottom: "2rem" }}>
      <Card.Img variant="top" src={`https://imagedelivery.net/1RellDI3Bs0xerXj5L6RyQ/${tokenId}/public`} />
      <Card.Body>
        <p>{tokenId}</p>
        <Button onClick={() => unStakeNft(tokenId)} variant="danger">UNSTAKE</Button>
      </Card.Body>
    </Card>
    )
}

export default NFTStakedCard;
