import React from "react";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

const NFTCard = ({nft, stakeNft}) => {
    return (
    <Card  style={{ width: '20rem', display: "inline-block", marginRight: "2rem", marginBottom: "2rem" }}>
      {/* <Card.Img variant="top" src="https://tinhhoabacbo.com/wp-content/uploads/2021/12/cute-pho-mai-que.jpg" /> */}
      <Card.Img variant="top" src={`https://imagedelivery.net/1RellDI3Bs0xerXj5L6RyQ/${nft.token_id}/public`} />

      <Card.Body>
        <p>{nft.token_id}</p>
        <Button onClick={() => stakeNft(nft.token_id)} variant="success">STAKE NFT</Button>
      </Card.Body>
    </Card>
    )
}

export default NFTCard
