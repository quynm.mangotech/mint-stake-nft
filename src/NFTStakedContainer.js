import React from "react";
import NFTStakedCard from "./NFTStakedCard";


const NFTStakedContrainer = ({nftStaked,unStakeNft}) => {
    return (
        <div>
            {nftStaked.map((tokenId, index) => {
                return <NFTStakedCard tokenId={tokenId} key={index} unStakeNft={unStakeNft}/>
            })}
        </div>
    )
}

export default NFTStakedContrainer;