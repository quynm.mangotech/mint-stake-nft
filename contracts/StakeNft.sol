// SPDX-License-Identifier: MIT
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

pragma solidity ^0.8.4;

contract StakeNft {
    IERC721 public parentNFT;
    IERC20 public tokenILM;
    
    address payable owner;
    struct Stake {
        uint256 tokenId;
        uint256 timestamp;
    }
    
    mapping(address => Stake[] ) public stakes;
    mapping(uint256 => bool) public isStaked;
    mapping(uint256 => uint256) public stakingTime; 
    mapping(uint => address) public ownerNft;

    constructor(address _addressMintNft, address _addressTokenERC20) {
        owner = payable(msg.sender);
        parentNFT = IERC721(_addressMintNft);
        tokenILM = IERC20(_addressTokenERC20);
    }

    function stake(uint256 _tokenId) public {
        require(parentNFT.ownerOf(_tokenId) == msg.sender, "StakeNft: You are not owner this NFT");
        require(isStaked[_tokenId] == false, "StakeNft: Item has been staked");
        parentNFT.safeTransferFrom(msg.sender, address(this), _tokenId, "0x00");
        Stake memory newStake = Stake(_tokenId, block.timestamp);
        stakes[msg.sender].push(newStake);
        isStaked[_tokenId] = true;
        ownerNft[_tokenId] = msg.sender;
    }

    function unstake(uint _tokenId) public payable{
        require(ownerNft[_tokenId] == msg.sender, "StakeNft: You are not owner this NFT");
        require(isStaked[_tokenId] == true, "StakeNft: Item has not been staked");

        for(uint256 i; i < stakes[msg.sender].length; i++) {
            if(stakes[msg.sender][i].tokenId == _tokenId) {
                parentNFT.safeTransferFrom(address(this), msg.sender, stakes[msg.sender][i].tokenId, "0x00");
                stakingTime[_tokenId] += (block.timestamp - stakes[msg.sender][i].timestamp);
                if(stakingTime[_tokenId] >= 300) {
                    tokenILM.transferFrom(owner, msg.sender, 1*10**18);
                }
                stakes[msg.sender][i] = stakes[msg.sender][stakes[msg.sender].length -1];
                stakes[msg.sender].pop();
                isStaked[_tokenId] = false;
                break;
            }
        }
    }
    function getListStaked() public view  returns(uint[] memory){
        uint256 currentIndex = 0;
        uint length = stakes[msg.sender].length;
        uint[] memory listToken = new uint[](length);

        for(uint256 i = 0; i< length; i++ ) {
            listToken[currentIndex] = stakes[msg.sender][i].tokenId;
            currentIndex++;
        }
        return listToken;
    }

    function onERC721Received(
        address operator,
        address from,
        uint256 id,
        bytes calldata data
    ) external returns (bytes4) {
        return bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"));
    }
}

