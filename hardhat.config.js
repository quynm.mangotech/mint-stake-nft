require("@nomiclabs/hardhat-waffle");
/**
 * @type import('hardhat/config').HardhatUserConfig
 */
 const ALCHEMY_API_KEY = "ym7cvUAGUg_nMmX3lQW50VFBwJuNbd96";
 const PRIVATE_KEY = "2892f301795ca80180eb1d7137fa0b112e473b8e5b9b45cf32f9ac1e2385d21d";
module.exports = {
  solidity: "0.8.4",
  path: {
    artifacts: './src/artifacts'
  },
  defaultNetwork: "bsc",
  networks: {
    hardhat: {
      chainId: 1337
    },
    rinkeby: {
      url: `https://eth-rinkeby.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
      accounts: [`${PRIVATE_KEY}`]
    },
    bsc: {
      url: "https://data-seed-prebsc-1-s1.binance.org:8545",
      chainId: 97,
      gasPrice: 20000000000,
      accounts: [`${PRIVATE_KEY}`],
    }
  },
  // path: {
  //   artifacts: "./src/artifacts"
  // }
};
